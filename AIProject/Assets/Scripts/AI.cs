﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
public class AI : MonoBehaviour {

	// Use this for initialization
	private NavMeshAgent theAgent;
	public Transform nextTarget;
	public Transform[] Points;
	private int CountPoints = 0;
    int timePatrolling = 10;
    public struct PathPoint{
		public Transform transform;
		public bool done;
	};
	public PathPoint[] Paths;
    AudioSource tmp;
	public enum StateMachine
	{
        idle,
		patrol,
		follow,
        relax,
        alarm,
        dead
	};


    public StateMachine myState;
	float timeCount = 4.0f;
    bool isalive= true;
    Animator anim;
	void Start () {
		//Paths = new PathPoint[4];
		theAgent = GetComponent<NavMeshAgent>();
        //for(int i = 0; i < Points.Length; i++)
        //{
        //	Paths[i].transform = Points[i].transform;
        //}
        myState = StateMachine.idle;
        tmp = GetComponent<AudioSource>();
        isalive = true;
        anim = GetComponent<Animator>();
        timePatrolling = Random.Range(20,60);
        StartCoroutine(LoseTime());
	}
	
  
	// Update is called once per frame
	void Update () {
        if (timePatrolling < 0)
        {
            //myState = StateMachine.relax;
        }
		ControllingZone();
        CheckAlive();

        if (Vector3.Distance(transform.position, nextTarget.position) < 20.0f)
        {
            if (nextTarget.GetComponent<Player>().IsMakingNoise() == true)
            {
                myState = StateMachine.follow;
                if (Physics.Linecast(transform.position, nextTarget.position))
                {
                    Debug.Log("blocked");
                }
                else
                {
                    myState = StateMachine.follow;
                }
                
            }
        }

		switch (myState)
		{
            case StateMachine.idle:
                Idle();
                break;
            case StateMachine.patrol:
				Patrolling();
				break
				;
			case StateMachine.follow:
				FollowingPlayer();
				break
				;
            case StateMachine.relax:
                Relax();
                break
                ;
            case StateMachine.alarm:
                ActivateAlarm();
                break
                ;
            case StateMachine.dead:
                Dead();
                break;

		}
	 

	}

    void ActivateAlarm()
    {

            GameObject positionAlarm = GameObject.FindGameObjectWithTag("Alarm1");
        if (positionAlarm.GetComponent<Alarm>().IsActivated() == false)
        {

            if (Vector3.Distance(transform.position, positionAlarm.transform.position) < 20.0f)
            {

                theAgent.SetDestination(positionAlarm.transform.position);
                if (Vector3.Distance(transform.position, positionAlarm.transform.position) < 3.0f)
                {
                    //GameObject obj = GameObject.FindGameObjectWithTag("Alarm1");
                    positionAlarm.GetComponent<Alarm>().SetAlarm();
                    myState = StateMachine.follow;
                }
            }
            else
            {
                myState = AI.StateMachine.follow;

            }
        }


        
    }

    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timePatrolling--;
        }
    }
    IEnumerator TimeRelaxed()
    {
        yield return new WaitForSeconds(5);
        timePatrolling = Random.Range(30, 60); ;
        myState = StateMachine.patrol;
        doonceRelax = false;
        StartCoroutine(LoseTime());
        
    }

    public void SetPoints( Transform[] tmp ,int size) 
    {
        Points = new Transform[size];
        Points = tmp;
        Paths = new PathPoint[size];
        for (int i = 0; i < Points.Length; i++)
        {
            Paths[i].transform = Points[i].transform;
            
        }
        myState = StateMachine.patrol;
        //print(myState);
       // anim.SetBool("isPatrolling", true);
    }
    void CheckAlive()
    {
        if (isalive == false)
        {
            myState = StateMachine.dead;
        }
    }
    bool doonceRelax = true;
    void Relax()
    {
        Vector3 savePosRelax= new Vector3();
        if (timePatrolling < 0)
        {
            GameObject tmp = GameObject.FindGameObjectWithTag("RelaxSpawn");
            theAgent.SetDestination(tmp.transform.position);
            savePosRelax = tmp.transform.position;
        }

        if(Vector3.Distance(transform.position, savePosRelax) <2.0f)
        {
            if (doonceRelax == true)
            {
                doonceRelax = false;
                StartCoroutine(TimeRelaxed());
            }
         
        }
       
    }
    public void SetStateExtern(StateMachine fms)
    {
        myState = fms;
    }

    public void SetState(StateMachine tmp)
    {
        myState = tmp;
    }
    void Dead()
    {
        theAgent.isStopped = true;
        theAgent.velocity = Vector3.zero;
        GetComponent<Animator>().SetBool("isAlive", false);
        StartCoroutine(DeleteObject());
    }
    public void  SetAlive(bool tmp)
    {
         isalive = tmp; 
    }
	void ResetPaths()
	{
		for(int i = 0; i < Paths.Length; i++)
		{
			Paths[i].done = false;
		}
	}

    void Idle()
    {
        myState = StateMachine.patrol;
    }

   void FollowingPlayer()
	{
        //print("Persiguiendo");
       
        theAgent.SetDestination(nextTarget.position);
        if (Vector3.Distance(transform.position, nextTarget.position) < 2.0f)
        {
           // print(Application.dataPath);
            SceneManager.LoadScene("Menu");
        }
        
        //transform.LookAt(nextTarget.position);
	}
	void Patrolling()
	{
        anim.SetBool("isPatrolling", true);
        //print("Patrullando");
        if (tmp.isPlaying == false)
        {
            tmp.Play();
        }
        //print(CountPoints);
       
        if (Paths != null)
        {
            if (Paths[CountPoints].done == false)
            {
                theAgent.SetDestination(Paths[CountPoints].transform.position);
                //transform.LookAt(Paths[CountPoints].transform.position);

                //if (Paths[CountPoints].transform.position.x == transform.position.x &&
                // Paths[CountPoints].transform.position.z == transform.position.z)
                if (Vector3.Distance(Paths[CountPoints].transform.position, transform.position) < 2.0f)
                {
                    print("Estoy en posicion");
                    Paths[CountPoints].done = true;
                    if (CountPoints < Paths.Length)
                    {
                        CountPoints++;
                        print("Aumenta");
                        if (CountPoints >= Paths.Length)
                        {
                            ResetPaths();
                            CountPoints = 0;

                        }

                    }
                    else
                    {
                        CountPoints = 0;
                        print("Se resetea");
                    }

                }
            }
            else
            {


            }
        }
	}
    IEnumerator DeleteObject()
    {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject, 1.0f);
    }
	void ControllingZone()
	{
        float distance= 10.0f;
        if (isalive == true)
        {
            RaycastHit hit;
            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            Debug.DrawRay(transform.position, fwd * distance);
            Debug.DrawRay(transform.position, (transform.forward + transform.right).normalized * distance);
            Debug.DrawRay(transform.position, (transform.forward - transform.right).normalized * distance);

            if (Physics.Raycast(transform.position, fwd, out hit, distance))
            {
               // print("Enfrente");
                if (hit.collider.gameObject.tag == "Player")
                {
                    myState = StateMachine.alarm;
                    nextTarget = hit.transform;
                }
            }
            else if (Physics.Raycast(transform.position, (transform.forward + transform.right).normalized, out hit, distance))
            {
               // print("Derecha");
                if (hit.collider.gameObject.tag == "Player")
                {
                    myState = StateMachine.alarm;
                    nextTarget = hit.transform;
                }
            }
            else if (Physics.Raycast(transform.position, (transform.forward - transform.right).normalized, out hit, distance))
            {
              //  print("Izquierda");
                if (hit.collider.gameObject.tag == "Player")
                {
                    myState = StateMachine.alarm;
                    nextTarget = hit.transform;
                }
            }
            else
            {
                if (myState == StateMachine.follow)
                {
                    timeCount -= Time.deltaTime;

                    if (timeCount <= 0.0f)
                    {
                        print("TIMER ENDED");
                        myState = StateMachine.patrol;
                        timeCount = 4.0f;
                    }
                }
            }
        }
	}


	void OnCollisionEnter(Collision col)
	{
		print("Enter collision");
		//	Destroy(col.gameObject);
	}
}
