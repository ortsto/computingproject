﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;

    public Vector3 offset;
    public float smoothSpeed = 2f;

    public float currentZoom = 1f;
    public float maxZoom = 3f;
    public float minZoom = .3f;
    public float yawSpeed = 70;
    public float zoomSensitivity = .7f;
    float dst;

    float zoomSmoothV;
    float targetZoom;
    //Switch Material
    public Material[] tmpSave;
    public Material transMat;
    GameObject hitGO = null;
    int selectedMaterial;
    void Start()
    {
        //tmpSave = new Material[4];
        dst = offset.magnitude;
        transform.LookAt(target.transform);
        targetZoom = currentZoom;
        selectedMaterial = 0;
    }

    void Update()
    {
        float scroll = Input.GetAxisRaw("Mouse ScrollWheel") * zoomSensitivity;

        if (scroll != 0f)
        {
            targetZoom = Mathf.Clamp(targetZoom - scroll, minZoom, maxZoom);
        }
        currentZoom = Mathf.SmoothDamp(currentZoom, targetZoom, ref zoomSmoothV, .15f);
        float Distance = Vector3.Distance(transform.position, target.position);
        RaycastHit hit;
        Vector3 position = GetComponentInChildren<Transform>().position;
        
        if (Physics.Raycast(position, transform.forward, out hit ,Distance-1.0f)){
            //print(hit.collider.name);
            if(hit.collider.tag == "AICharacter" || hit.collider.tag == "Player" || hit.collider.tag == "Ground")
            {

            }else
            {
                hitGO = GameObject.Find(hit.collider.name);
                if(hitGO.tag == "OutWall")
                {
                    selectedMaterial = 0;
                }
                else if(hitGO.tag == "Laberinto")
                {
                    selectedMaterial = 1;
                }
                hitGO.GetComponent<Renderer>().material = transMat;
            }
          
        }else
        {
            if (hitGO != null)
            {
                hitGO.GetComponent<Renderer>().material = tmpSave[selectedMaterial];
                hitGO = null;
            }
        }

        
    }

    void LateUpdate()
    {

        transform.position = target.position - transform.forward * dst * currentZoom;
        transform.LookAt(target.position);

        float yawInput = Input.GetAxisRaw("Horizontal");
        transform.RotateAround(target.position, Vector3.up, -yawInput * yawSpeed * Time.deltaTime);

        //transform.position = target.position - transform.forward * dst * currentZoom;
        //transform.LookAt(target.position);

    }
}
