﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LineParcel : MonoBehaviour {
    public enum Directions
    {
        Forward,Back,Left,Right
    };
    public Directions dirr;
    private LineRenderer lineRender;
    private Vector3 [] curreDirr;
    private Vector3 startPosition;
    private Vector3 endPosition;
    public Vector3 targetPos;
    bool activator;
	// Use this for initialization
	void Start () {
        curreDirr = new Vector3[4];
        curreDirr[0] = Vector3.forward;
        curreDirr[1] = Vector3.back;
        curreDirr[2] = Vector3.left;
        curreDirr[3] = Vector3.right;
        lineRender = GetComponent<LineRenderer>();
        startPosition = transform.position;
        endPosition = targetPos;
        activator = true;
        GetComponent<AudioSource>().Play();
    }
	
	// Update is called once per frame

    public void Deactivator()
    {
        print("Desactivate");
        activator = false;
        lineRender.enabled = false;
    }

    public void SetLaserDirection(int tmp)
    {
        lineRender.SetPosition(0, transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position,curreDirr[tmp], out hit, 20.0f))
        {
            
            lineRender.enabled = true;
            lineRender.SetPosition(1, hit.point);
            if(hit.collider.tag== "Player")
            {
                SceneManager.LoadScene("Menu");
            }
            if (hit.collider.tag == "AICharacter")
            {
                print("Se muere por "+transform.name);
                GameObject.Find(hit.collider.name).GetComponent<AI>().SetAlive(false);
            }
            // print("Found an object - distance: " + hit.distance);
        }
        else
        {
            lineRender.enabled = false;
        }
    }
	void Update () {

        if (activator == true)
        {
            SetLaserDirection((int)dirr);
            if (transform.position == endPosition)
            {
                //print("Llego !");
                targetPos = startPosition;
            }
            else if (transform.position == startPosition)
            {
                //print("Comienza");
                targetPos = endPosition;
            }
            transform.position = Vector3.MoveTowards(transform.position, targetPos, 5.0f * Time.deltaTime);
        }
    }
}
