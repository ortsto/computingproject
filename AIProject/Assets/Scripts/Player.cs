﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    public List<string> inventory_;
    Animator anim;
    NavMeshAgent mNavMesh;
    public static bool GamePause = false;
   void Start()
    {
        inventory_ = new List<string>();
        addObject("Pelota");
        addObject("Cabezon");
        anim = GetComponentInChildren<Animator>();
        mNavMesh = GetComponent<NavMeshAgent>();
    }

    void addObject(string name)
    {
        inventory_.Add(name);
    }
    void deleteObject(string name)
    {
        inventory_.Remove(name);
       
    }
    bool useObject()
    {
        return inventory_.Contains(name);
    }

    void ResetScene()
    {
        SceneManager.LoadScene("Menu");
    }


    public bool IsMakingNoise()
    {
        return GetComponent<AudioSource>().isPlaying;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bullet" || collision.transform.tag == "CustomAI")
        {
            //print("Entras");
            SceneManager.LoadScene("Menu");
        }
    }


    void Update()
    {
        if (GamePause == false)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit, 100))
                {
                    mNavMesh.destination = hit.point;
                }
            }
            if (Vector3.Distance(transform.position, mNavMesh.destination) < 2.0f)
            {
                mNavMesh.velocity = Vector3.zero;
            }
            if (mNavMesh.velocity.x != 0.0f && mNavMesh.velocity.z != 0.0f)
            {
                anim.SetBool("KeyPress", true);
                if (GetComponent<AudioSource>().isPlaying == false && anim.GetBool("ControlPress") == false)
                {
                    GetComponent<AudioSource>().Play();
                }
            }
            else
            {
                anim.SetBool("KeyPress", false);
            }
            if (Input.GetKey(KeyCode.LeftShift))
            {
                anim.SetBool("ShiftPress", true);
            }
            else
            {
                anim.SetBool("ShiftPress", false);
            }
            if (Input.GetKey(KeyCode.LeftControl))
            {
                anim.SetBool("ControlPress", true);

            }
            else
            {
                anim.SetBool("ControlPress", false);
            }
            if (Input.GetKey(KeyCode.Space))
            {
                anim.SetBool("SpacePress", true);
            }
            else
            {
                anim.SetBool("SpacePress", false);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GamePause)
            {
                Time.timeScale = 1.0f;
                GamePause = false;
            }
            else
            {
                Time.timeScale = 0.0f;
                GamePause = true;
                GetComponent<AudioSource>().Stop();
            }
            
        }

            //float moveVertical = Input.GetAxisRaw("Vertical");
            //float moveHorizontal = Input.GetAxisRaw("Horizontal");
            //if (moveVertical != 0.0f)
            //{
            //    anim.SetBool("KeyPress",true);
            //    if (GetComponent<AudioSource>().isPlaying == false && anim.GetBool("ControlPress") == false)
            //    {
            //        GetComponent<AudioSource>().Play();
            //    }
            //}else
            //{
            //    anim.SetBool("KeyPress", false);
            //}
            //if(Input.GetKey(KeyCode.LeftShift)){
            //    anim.SetBool("ShiftPress", true);
            //}
            //else
            //{
            //    anim.SetBool("ShiftPress", false);
            //}
            //if (Input.GetKey(KeyCode.LeftControl))
            //{
            //    anim.SetBool("ControlPress", true);

            //}
            //else
            //{
            //    anim.SetBool("ControlPress", false);
            //}
            //if (Input.GetKey(KeyCode.Space))
            //{
            //    anim.SetBool("SpacePress", true);
            //}
            //else
            //{
            //    anim.SetBool("SpacePress", false);
            //}

            //Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            //transform.Translate(transform.TransformDirection(movement) * 3.0f * Time.deltaTime, Space.World);
            //transform.Rotate(0, moveHorizontal, 0);

        }
    }

