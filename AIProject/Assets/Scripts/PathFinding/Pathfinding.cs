﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pathfinding : MonoBehaviour {


	public Transform target;
	Grid grid;
	public Direction PathDirection = Direction.k_4Directions;

	public enum Direction
	{
		k_4Directions, k_8Directions
	}

	private void Awake()
	{
		grid = GameObject.Find("PathFinding").GetComponent<Grid>();
		
	}
	void Start()
{
    
       }

	public Vector2 SizeMap()
	{
		return new Vector2(-grid.gridSizeX/2, grid.gridSizeY/2);
	}

	public List<Node> Search(Vector3 startVector, Vector3 endVector)
	{
		Node start = grid.NodeFromWorldPoint(startVector);
		Node end = grid.NodeFromWorldPoint(endVector);

		List<Node> OpenList = new List<Node>();
		//no duplicates
		HashSet<Node> ClosedList = new HashSet<Node>();
		Node current = null;
		OpenList.Add(start);
		//Nodepq[pqi].push(*start);
		start.CalculoH((int) endVector.x,(int) endVector.y);
		start.G = 0;
		start.CalculoF();
		List<Node> Paths = null;
		while (OpenList.Count != 0)
		{
			current = OpenList[0];
			for (int i = 1; i<OpenList.Count; i++)
			{

				if (current.F<OpenList[i].F)
				{
					current = OpenList[i];
				}

			}

			OpenList.Remove(current);
			ClosedList.Add(current);
			if (current == end)
			{
				Paths = new List<Node>();
				Node currentNode = end;
				while (currentNode != start)
				{
					Paths.Add(currentNode);
					currentNode = currentNode.parent;
				}
				Paths.Reverse();
				grid.path = Paths;
				return Paths;
			}

            //Check neighbour

            foreach (Node neighbour in grid.GetNeighbours(current))
            {
                if (!neighbour.walkable || ClosedList.Contains(neighbour))
                {
                    continue;
                }

                int CostToNeighbour = current.G + current.CalculoH(neighbour.gridX, neighbour.gridY);
                if (CostToNeighbour<neighbour.G || !OpenList.Contains(neighbour))
                {
                    neighbour.G = CostToNeighbour;
                    neighbour.G = neighbour.CalculoH(end.gridX, end.gridY);
                    neighbour.parent = current;

                    if (!OpenList.Contains(neighbour))
                        OpenList.Add(neighbour);
                }
            }
            //foreach (Node neighbour in NearestNodes(current))
            //{
            //    if (!neighbour.walkable || ClosedList.Contains(neighbour))
            //    {
            //        continue;
            //    }

            //    int CostToNeighbour = current.G + current.CalculoH(neighbour.gridX, neighbour.gridY);
            //    if (CostToNeighbour < neighbour.G || !OpenList.Contains(neighbour))
            //    {
            //        neighbour.G = CostToNeighbour;
            //        neighbour.G = neighbour.CalculoH(end.gridX, end.gridY);
            //        neighbour.parent = current;

            //        if (!OpenList.Contains(neighbour))
            //            OpenList.Add(neighbour);
            //    }
            //}
        }
		
		return null;
	  
	}
	private List<Node> NearestNodes(Node current)
	{
		List<Node> vecinos = new List<Node>();
		if (PathDirection == Direction.k_4Directions)
		{
			Vector2[] Dir = new Vector2[]{
               new Vector2(-1, 0),
               new Vector2(0,  1),
               new Vector2(1, 0),
               new Vector2(0, -1)
               };
			
			for (int i = 0; i<Dir.Length; i++)
			{
				int checkX = current.gridX + (int)Dir[i].x;
				int checkY = current.gridY + (int)Dir[i].y;

				if (checkX >= 0 && checkX<grid.gridSizeX && checkY >= 0 && checkY<grid.gridSizeY)
				{
					vecinos.Add(grid.grid[checkX, checkY]);
				}
			}
			
			return vecinos;
		}
		else
		{
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    int checkX = current.gridX + x;
                    int checkY = current.gridY + y;

                    if (checkX >= 0 && checkX<grid.gridSizeX && checkY >= 0 && checkY<grid.gridSizeY)
                    {
                        vecinos.Add(grid.grid[checkX, checkY]);
                    }
                }
            }
            return vecinos;
		}
	}

	// Update is called once per frame
	void Update()
    {
    
    }


}
