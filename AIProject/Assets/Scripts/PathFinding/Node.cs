﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node  {

    
    public bool walkable;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;
    //int level;
    //int priority; 
    public int G { get; set; }
    public int H { get; set; }
    public int F { get; set; }
    public Node parent;
    public bool done;
    public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY)
    {
        walkable = _walkable;
        worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
        parent = null;
        done = false;
    }


    public int CalculoH(int xDest, int yDest)
    {
        int xd, yd;
        xd = (xDest - gridX) * 10;
        yd = (yDest - gridY) * 10;
        H = Mathf.Abs(xd) + Mathf.Abs(yd);
        return H;
    }
    
    public void CalculoG(int dia)
    {
        int sum = dia < 4 ? 10 : 14;
        G += parent.G = sum;
    }
    public void CalculoF()
    {
        F = G + H;
    }
}
