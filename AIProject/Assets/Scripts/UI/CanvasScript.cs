﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasScript : MonoBehaviour {

    // Use this for initialization
    public Text move;
    public Text run;
    public Text crouch;
    public Text timeCount;
    public Text pauseText;
    public int TimeLeft = 120;
    void Start () {
        move.enabled = false;
        run.enabled = false;
        crouch.enabled = false;
        StartCoroutine(LoseTime());
    }


    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            TimeLeft--;
        }
    }

    public void Activate(int selected)
    {
        if (selected == 0)
        {
            move.enabled = true;
            run.enabled = false;
            crouch.enabled = false;
        }
        else if (selected == 1)
        {
            move.enabled = false;
            run.enabled = true;
            crouch.enabled = false;
        }
        else if (selected == 2)
        {
            move.enabled = false;
            run.enabled = false;
            crouch.enabled = true;
        }
        else
        {
            move.enabled = false;
            run.enabled = false;
            crouch.enabled = false;
        }
    }
	// Update is called once per frame
	void Update () {
        timeCount.text = ("Time " + TimeLeft);
        if (Time.timeScale == 0)
        {
            pauseText.enabled = true;
        }else
        {
            pauseText.enabled = false;
        }
        if(TimeLeft<=0){
            SceneManager.LoadScene("Menu");
        }
    }
}
