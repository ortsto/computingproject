﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {
    public Button playButton;
    public Button exitButton;
    // Use this for initialization
    void Start () {
        Button pbt = playButton.GetComponent<Button>();
        pbt.onClick.AddListener(PlayButton);
        Button ebt = exitButton.GetComponent<Button>();
        ebt.onClick.AddListener(ExitButton);
    }
	
    void PlayButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("First");
    }
    void ExitButton()
    {
        Application.Quit();
    }
	// Update is called once per frame
	void Update () {
		
	}
}
