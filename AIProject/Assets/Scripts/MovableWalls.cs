﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableWalls : MonoBehaviour {

    private Vector3 startPosition;
    private Vector3 endPosition;
    public Vector3 targetPos;
    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;
        endPosition = targetPos;
    }

    // Update is called once per frame


    void Update()
    {

        if (transform.position == endPosition)
        {
            //print("Llego !");
            targetPos = startPosition;
        }
        else if (transform.position == startPosition)
        {
            //print("Comienza");
            targetPos = endPosition;
        }
        transform.position = Vector3.MoveTowards(transform.position, targetPos, 5.0f * Time.deltaTime);
        
    }
}
