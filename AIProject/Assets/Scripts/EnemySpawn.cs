﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySpawn : MonoBehaviour {


    public GameObject prefab;
    public Transform playerT;
    public int enemySize;
    public List<Transform> list;

    GameObject[] enemyArray;
	void Start () {
        
        enemyArray = new GameObject[enemySize];
        for(int i = 0; i < enemySize; i++)
        {
            enemyArray[i] = Instantiate(prefab, transform.position,transform.rotation);
            enemyArray[i].GetComponent<AI>().SetPoints(makeRandomOrder(), list.Count);
            enemyArray[i].transform.tag = "AICharacter";
            enemyArray[i].transform.name = "AI" + i;
            enemyArray[i].GetComponent<AI>().nextTarget = playerT;
        }
	}
	

    Transform[] makeRandomOrder()
    {
        Transform[] tmpPoints = new Transform[list.Count];

        int randomOption = Random.Range(0, 1);
        List<Transform> tmpList = list;
        if (randomOption == 0)
        {
            
            for(int i = 0; i < list.Count; i++)
            {
                tmpPoints[i] = tmpList[i];
            }
        }
        else if(randomOption == 1)
        {
            tmpList.Reverse();
            for (int i = 0; i < list.Count; i++)
            {
                tmpPoints[i] = tmpList[i];
            }
        }
        else if (randomOption == 2)
        {
            for(int i = 0; i < list.Count; i++)
            {
                int randomPoint = Random.Range(0, list.Count);
                tmpPoints[i] = tmpList[randomPoint];
            }
        }
        else 
        {
            for (int i = 0; i < list.Count; i++)
            {
                int randomPoint = Random.Range(0, list.Count);
                tmpPoints[i] = tmpList[randomPoint];
            }
        }

        return tmpPoints;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
