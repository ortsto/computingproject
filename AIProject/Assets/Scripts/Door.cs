﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    // Use this for initialization
    bool isTriggering;
    AudioSource src ;
    Animator anim;
    void Start () {
        src = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider other)
    {
        anim.SetTrigger("OpenDoor");
        GetComponent<Renderer>().enabled = false;
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }
    void OnTriggerExit(Collider other)
    {
        anim.enabled = true;
        GetComponent<Renderer>().enabled = true;
        gameObject.GetComponent<BoxCollider>().enabled = true;
    }

    void PlaySound()
    {
        src.Play();
        
    }

    void PauseAnimationEvent()
    {
        anim.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
