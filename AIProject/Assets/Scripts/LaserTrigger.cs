﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTrigger : MonoBehaviour {

    public GameObject laserPar;
    bool canbePressed;
	// Use this for initialization
	void Start () {
	    
	}
	
    void OnTriggerEnter(Collider colli)
    {
        if (colli.tag == "Player")
            canbePressed = true;
    }
    void OnTriggerExit(Collider colli)
    {
        if (colli.tag == "Player")
            canbePressed = false;
    }
    // Update is called once per frame
    void Update () {
		if(canbePressed == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                laserPar.GetComponent<LineParcel>().Deactivator();
            }
        }
	}
}
