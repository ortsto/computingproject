﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AIAgentPathCustom : MonoBehaviour {

    // Use this for initialization
    Pathfinding path;
    public List<Node> pathFollow;
    int currentPath;
    public Transform target;
    Collider m_Collider;
    RaycastHit m_Hit;
    bool m_HitDetect;
    public Vector3 sizeCubeDetection;
    NavMeshAgent agentMesh;

    public AI.StateMachine stateAgent;

    void Start() {
        path = GameObject.Find("PathFinding").GetComponent<Pathfinding>();
        pathFollow = path.Search(transform.position, new Vector3(0, 1, 0));
        currentPath = 0;
        stateAgent = AI.StateMachine.patrol;
        m_Collider = GetComponent<Collider>();
        agentMesh = GetComponent<NavMeshAgent>();
    }


    bool stopDoing = false;
    bool Follow = false;

    bool imcreatingpath = false;
   

    IEnumerator SearchNewPath(Vector3 tmp)
    {
        if (imcreatingpath == false)
        {
            imcreatingpath = true;
            yield return new WaitForSeconds(1);

            pathFollow = new List<Node>();
            pathFollow = path.Search(transform.position, tmp);
            imcreatingpath = false;
            currentPath = 0;

            

        }

     


    }

    IEnumerator CheckRest()
    {

        yield return new WaitForSeconds(15);
    }


    void RandomPath()
    {
        if (stopDoing == false && pathFollow !=null)
        {
            if (Vector3.Distance(transform.position, pathFollow[currentPath].worldPosition) < 1.0f)
            {

                currentPath += 1;
                if (currentPath >= pathFollow.Count)
                {
                    currentPath = 0;
                    stopDoing = true;
                }


            }
            //print(pathFollow[currentPath].worldPosition);
            transform.LookAt(pathFollow[currentPath].worldPosition);
            transform.position = Vector3.MoveTowards(transform.position, pathFollow[currentPath].worldPosition, 2.0f * Time.deltaTime);

        }
        else
        {
            //Creation of new path 
            StartCoroutine(SearchNewPath(new Vector3(Random.Range(-35, 35), 1, Random.Range(-90, 90))));
            stopDoing = false;
        }
    }

    void ResetToPatrol()
    {
        stopDoing = true;
        pathFollow = null;
    }
    public void SetStateExtern(AI.StateMachine fms )
    {
        stateAgent = fms;
    }
    void Chase()
    {


        agentMesh.SetDestination(target.position);
        if (Vector3.Distance(transform.position, target.position) > 20.0f)
        {
            stateAgent = AI.StateMachine.patrol;
            ResetToPatrol();
            

        }
    }


    void FollowPath()
    {
        if (Follow == false)
        {
            transform.LookAt(pathFollow[currentPath].worldPosition);
            transform.position = Vector3.MoveTowards(transform.position, pathFollow[currentPath].worldPosition, 2.0f * Time.deltaTime);
            if (Vector3.Distance(transform.position, pathFollow[currentPath].worldPosition) < 1.0f)
            {

                currentPath += 1;
                if (currentPath >= pathFollow.Count)
                {
                    currentPath = 0;

                    Follow = true;
                }


            }


        }
        else
        {
            GameObject obj = GameObject.FindGameObjectWithTag("Alarm");
            obj.GetComponent<Alarm>().SetAlarm();
            transform.LookAt(target);
            stateAgent = AI.StateMachine.follow;
        }
    }
    bool firstTime = true;
    void SoundAlarm()
    {
        if (firstTime == true)
        {
            Vector3 positionAlarm = GameObject.FindGameObjectWithTag("Alarm").transform.position;
            if (Vector3.Distance(transform.position, positionAlarm) < 20.0f)
            {
                StartCoroutine(SearchNewPath(positionAlarm));
                firstTime = false;
            }else
            {
                stateAgent = AI.StateMachine.follow;

            }
          
        }
        

        FollowPath();
        
    }

	void Update () {
       

        if (stateAgent == AI.StateMachine.patrol)
        {
            if (Physics.BoxCast(m_Collider.bounds.center, sizeCubeDetection, transform.forward, out m_Hit, transform.rotation, 10.0f))
            {
                //Debug.Log("Hit : " + m_Hit.collider.name);
                if (m_Hit.transform.tag == "Player")
                {
                    stateAgent = AI.StateMachine.alarm;
                }
                m_HitDetect = true;
            }

            //if (Vector3.Distance(transform.position, target.position) < 15.0f)
            //{
            //    stateAgent = FSMCustom.Alarm;
            //}
        }
        if (Vector3.Distance(transform.position, target.position) < 10.0f)
        {
            if (target.GetComponent<Player>().IsMakingNoise() == true)
            {
                stateAgent = AI.StateMachine.follow;
            }
        }

        switch (stateAgent)
        {
            case AI.StateMachine.patrol:
                RandomPath();
                break;
            case AI.StateMachine.follow:
                Chase();
                break;
            case AI.StateMachine.alarm:
                SoundAlarm();
                break;
        }

       
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.name== "Player")
        {
            print("iscolliding");
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        
        if (m_HitDetect)
        {
            
            Gizmos.DrawRay(transform.position, transform.forward * m_Hit.distance);
           
            Gizmos.DrawWireCube(transform.position + transform.forward * m_Hit.distance, sizeCubeDetection);
        }
        
        else
        {
            
            Gizmos.DrawRay(transform.position, transform.forward * 10.0f);
           
            Gizmos.DrawWireCube(transform.position + transform.forward * 10.0f, sizeCubeDetection);
        }
    }
}
