﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour {

    // Use this for initialization
    public Material stateOn;
    public Material stateOff;
    public GameObject children;
    Renderer childrenAcces;
    AudioSource audioS;
    public bool testActivate = false;

	void Start () {
        childrenAcces = children.GetComponent<Renderer>();
        audioS = GetComponent<AudioSource>();
        audioS.loop = true;
    }

    IEnumerator ActivateAlarm()
    {
        audioS.Play();
        childrenAcces.material = stateOn;
        CheckClosestAI();
        yield return new  WaitForSeconds(10);
        childrenAcces.material = stateOff;
        audioS.Stop();


    }

    public void SetAlarm()
    {
        if (audioS.isPlaying == false)
        {
            StartCoroutine(ActivateAlarm());
        }
        
       
    }

    public bool IsActivated()
    {
        return audioS.isPlaying;
    }
    void CheckClosestAI()
    {
        GameObject[] listAI = GameObject.FindGameObjectsWithTag("CustomAI");
        {
            foreach(GameObject go in listAI)
            {
                if (go.GetComponents<AIAgentPathCustom>().Length != 0)
                {
                    float dist = Vector3.Distance(transform.position, go.transform.position);
                    if (dist < 30.0f)
                    {
                        go.GetComponent<AIAgentPathCustom>().SetStateExtern(AI.StateMachine.alarm);
                    }
                }

               
            }
        }
        GameObject[] listAI2 = GameObject.FindGameObjectsWithTag("AICharacter");
        {
            foreach (GameObject go in listAI2)
            {
                if (go.GetComponents<AIAgentPathCustom>().Length != 0)
                {
                    float dist = Vector3.Distance(transform.position, go.transform.position);
                    if (dist < 30.0f)
                    {
                        go.GetComponent<AI>().SetStateExtern(AI.StateMachine.alarm);
                    }
                }


            }
        }
    }
	// Update is called once per frame
	void Update () {
        if (testActivate == true)
        {
            testActivate = false;
            SetAlarm();
        }
        //childrenAcces.material = stateOff;
    }
}
