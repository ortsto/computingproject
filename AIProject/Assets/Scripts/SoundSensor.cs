﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSensor : MonoBehaviour {

    // Use this for initialization
    AI tmpAI;
	void Start () {
        tmpAI = GetComponent<AI>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player"){

            float distTwopoints = Vector3.Distance(transform.position, other.transform.position);
            var heading = transform.position - other.transform.position;

            var distance = heading.magnitude;
            var direction = heading / distance;
            RaycastHit hit;
            Physics.Raycast(transform.position, direction,out hit, distTwopoints);
            if(hit.transform.tag == other.tag)
            {
                tmpAI.SetState(AI.StateMachine.follow);

            }
            
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
