﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAI : MonoBehaviour {

    // Use this for initialization
    AIAgentPathCustom stateparent;
    AI stateparentOption;
    Renderer childrenAcces;
    int CaseOption;
    void Start () {

        if(gameObject.tag == "CustomAI")
        {
            stateparent = gameObject.GetComponentInParent<AIAgentPathCustom>();
            childrenAcces = GetComponent<Renderer>();
            CaseOption = 1;
        }
        else
        {
            stateparentOption = gameObject.GetComponentInParent<AI>();
            childrenAcces = GetComponent<Renderer>();
            CaseOption = 2;
        }
    }
	
	// Update is called once per frame

    void CaseOption1()
    {
        switch (stateparent.stateAgent)
        {
            case AI.StateMachine.patrol:
                childrenAcces.material.color = Color.green;
                break;
            case AI.StateMachine.follow:
                childrenAcces.material.color = Color.red;
                break;
            case AI.StateMachine.alarm:
                childrenAcces.material.color = Color.magenta;
                break;
        }
    }
    void CaseOption2()
    {
        switch (stateparentOption.myState)
        {
            case AI.StateMachine.patrol:
                childrenAcces.material.color = Color.green;
                break;
            case AI.StateMachine.follow:
                childrenAcces.material.color = Color.red;
                break;
            case AI.StateMachine.alarm:
                childrenAcces.material.color = Color.magenta;
                break;
            case AI.StateMachine.relax:
                childrenAcces.material.color = Color.cyan;
                break;
        }
    }
    void Update () {


        switch (CaseOption)
        {
            case 1: CaseOption1();
                break;

            case 2:  CaseOption2();
                break;
        }
	}
}
