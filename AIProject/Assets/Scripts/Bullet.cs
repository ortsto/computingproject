﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float speed = 8.0f;
    public Vector3 tar;
    float time = 5.0f;
	// Use this for initialization
	void Start () {
        //speed = 8.0f;
	}
	
    public void SetTarget(Vector3 target,float timeLive)
    {
        tar = target;
        time = timeLive;
    }
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, tar, speed * Time.deltaTime);
        Destroy(gameObject,time);
        
	}
}
