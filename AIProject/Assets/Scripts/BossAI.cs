﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAI : MonoBehaviour {

    // Use this for initialization
    public Transform target;
    public GameObject bullet;
    bool playerinRange;
    int ammoBoss;
    bool canShoot;

    void Start () {

        canShoot = true;

    }
    IEnumerator Example()
    {
        yield return new WaitForSeconds(1);
        canShoot = true; 
    }


    void ShootCircle()
    {
        if (canShoot ==true )
        {
            GameObject bull = Instantiate(bullet, transform.position, transform.rotation);
            bull.GetComponent<Bullet>().SetTarget(target.position,3.0f);
            ammoBoss++;
           
            canShoot = false;
            StartCoroutine(Example());
        }
       
    }

    void LaserAttack()
    {

           
    }

    void OnTriggerEnter(Collider other)
    {
        playerinRange = true;
        //StartCoroutine(Example());

    }
    void OnTriggerExit(Collider other)
    {
        playerinRange = false;

    }
    // Update is called once per frame
    void Update () {
        if (playerinRange == true)
        {
            float distanceVector = Vector3.Distance(transform.position, target.position);
            if (distanceVector < 20.0f)
            {
                Vector3 tmp = target.position;
                tmp.y = transform.position.y;
                transform.LookAt(tmp);
                ShootCircle();
            }
            else if (distanceVector > 20.0f)
            {
                LaserAttack();
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                ShootCircle();
            }
        }
	}
}
